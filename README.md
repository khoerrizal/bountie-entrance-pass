# Bountie Entrance Pass


## Description
This source code is the solution of problem statements in Bountie technical test. Each solution is separated in each folder inside. You can read technical explanation for every solution in sections below.

## Get started
Those solution is running under python 3.8. Please read the documentation how to install python 3.8 in https://www.python.org/downloads/release/python-380/.

Install required modules firstly.
```
pip install -r requirements.txt
```


## 1. Minion Games
Counting scores for Minion Games, between Kevin and Stuart.
Run minion game solution by call the module and string input as argument.

Parameter -i contain string input for the game.
Parameter -p contain string 2 player's name separated by a comma.
```
python 1_minion_game -i BANANA -p Stuart,Kevin
```

Here is the result of test cases.
```
$ python 1_minion_game -i BANANA -p Stuart,Kevin
Stuart score: 12
Kevin score: 9
Congratulation! Stuart is the winner.

```

```
$ python 1_minion_game -i TINTIN -p Stuart,Kevin
Stuart score: 14
Kevin score: 7
Congratulation! Stuart is the winner.
```

```
$ python 1_minion_game -i NANAS -p Stuart,Kevin
Stuart score: 9
Kevin score: 6
Congratulation! Stuart is the winner.
```

```
$ python 1_minion_game -i UAUUUOAAH -p Stuart,Kevin
Stuart score: 1
Kevin score: 44
Congratulation! Kevin is the winner.
```

```
$ python 1_minion_game -i DINGDINGDONGDINGTALK -p Stuart,Kevin
Stuart score: 155
Kevin score: 55
Congratulation! Stuart is the winner.
```

## 2. Visitor Counter
Manage visitor counter to provide data via API. Store log csv file in folder data. Any csv files in folder data will be read by app.

Run visitor counter service by call the module name.
```
python 2_visitor_counter
```

Use postman collection to check request and response example.

### a. Login
To access data, send login data first. For this solution, we use username batman and password Bountie!@#$

### b. Get sensor list
Get available sensor list from data.

### c. Get visitor data
Get all data by sensor id.

### d. Get visitor by hour
Get total visitor data by hour in specific date.

### e. Get visitor by date
Get total visitor data by hour in specific date or range of date.

## 3. URL Shortener
Generate short url for every request.

Run service by call the module and parameter host domain.
```
python -d https://rizal.com
```
Use postman collection to see request and response example.

### a. Create new shorten url
Send POST request with body data target.


### b. Get target url
Get request to query target url.