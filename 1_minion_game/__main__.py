import argparse
from collections import defaultdict
from pprint import pprint

VOWEL = list("AIUEO")


def main(s, player_name_list):
    """
    Player 1 compute substring that started with consonants.
    Player 2 compute substring that started with vowel.
    :param s: any string with alphabetical
    :return: string contains winner's name and score for every player. Example:
    """
    assert all([x.isalpha() and x.isupper() for x in s]), "String input can only upper case alphabet"
    player1, player2 = player_name_list
    player1_score = player2_score = 0
    vowel_substring = defaultdict(int)
    cons_substring = defaultdict(int)
    for i in range(len(s)+1):
        for x in range(len(s) + 1):
            substring = s[i:x]
            if not substring:
                continue
            if substring[0] in VOWEL:
                vowel_substring[substring] += 1
            else:
                cons_substring[substring] += 1
    # pprint(vowel_substring)
    # pprint(cons_substring)
    player1_score = sum([v for k, v in cons_substring.items()])
    player2_score = sum([v for k, v in vowel_substring.items()])
    print("{} score: {}".format(player1, player1_score))
    print("{} score: {}".format(player2, player2_score))
    if player1_score > player2_score:
        winner = player1
    elif player2_score > player1_score:
        winner = player2
    else:
        print("DRAW")
        return
    print("Congratulation! {} is the winner.".format(winner))
    return


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--input', help='String input', required=True)
    ap.add_argument('-p', '--player-name', help='Player 1 and player 2 name, separated by comma', required=True)
    args = ap.parse_args()
    str_input = args.input
    player_name = args.player_name
    assert "," in player_name, "Player name must contain player 1 and player 2 name separated by a comma"
    player_name = player_name.split(',')
    assert len(player_name) == 2, "Player name can only contains 2 names separated by a comma"
    main(str_input, player_name)
