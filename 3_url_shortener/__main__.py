import argparse
import uuid
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify, request

app = Flask(__name__)

db = SQLAlchemy(app)


class UrlDocs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url_target = db.Column(db.Text, nullable=False)
    url_shorten = db.Column(db.Text, nullable=False)


@app.route("/")
def hello_world():
    return "Welcome to url shortener service"


@app.route("/create", methods=["POST"])
def create_shorten_url():
    target = request.json.get("target", None)
    short_code = uuid.uuid4().hex[:6]
    url_shorten = "{}/{}".format(host_domain, short_code)
    db.create_all()
    db.session.add(UrlDocs(url_target=target, url_shorten=short_code))
    db.session.commit()
    return url_shorten


@app.route("/<url>")
def get_target_url(url):
    obj = UrlDocs.query.filter_by(url_shorten=url).one_or_none()
    if not obj:
        return jsonify("Invalid url"), 401
    return obj.url_target


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-d', '--Host-domain', help='String input', required=True)
    args = ap.parse_args()
    global host_domain
    host_domain = args.Host_domain
    db.create_all()
    db.session.commit()

    app.run()
