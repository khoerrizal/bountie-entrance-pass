import json
import os
import csv
from hmac import compare_digest
from pprint import pprint
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify
import pandas as pd

from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from flask import request

app = Flask(__name__)

# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = "65a1ae8a-2890-48f0-bc92-01d823c5036f"  # Change this!
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
jwt = JWTManager(app)
db = SQLAlchemy(app)


# in this solution, we set default password as "Bountie!@#$"
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text, nullable=False, unique=True)
    full_name = db.Column(db.Text, nullable=False)

    # NOTE: In a real application make sure to properly hash and salt passwords
    def check_password(self, password):
        return compare_digest(password, "Bountie!@#$")


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(username=identity).one_or_none()


@app.route("/")
def hello_world():
    return "Welcome to visitor counter service"


@app.route("/sensor")
@jwt_required()
def get_sensor_list():
    data = main()
    res = json.dumps(data.sensor_id.unique().tolist())
    return res


@app.route("/sensor/<int:sensor_id>")
@jwt_required()
def get_sensor_data_by_id(sensor_id):
    data = main()
    data_sensor = data[data['sensor_id'] == sensor_id]
    res = json.dumps(prepare_data(data_sensor))
    return res


@app.route("/sensor/<int:sensor_id>/hour/<date_str>")
def get_sensor_data_by_hour(sensor_id, date_str):
    data = main()
    date_obj = datetime.strptime(date_str, "%m-%d-%Y").date()
    data_sensor = data[data['sensor_id'] == sensor_id]
    data_sensor['date'] = data_sensor['timestamp'].apply(lambda x: x.date())
    data_sensor = data_sensor[data_sensor['date'] == date_obj]
    data_sensor['hour'] = data_sensor['timestamp'].apply(lambda x: x.hour)
    grouped = data_sensor.groupby('hour').sum()
    output_hour = []
    for key, val in grouped['id'].items():
        output_hour.append({
            'hour': key,
            'total': int(grouped['total'][key])
        })
    res = json.dumps(output_hour)
    return res


@app.route("/sensor/<int:sensor_id>/date/<start_date_str>")
def get_sensor_data_by_date(sensor_id, start_date_str):
    data = main()
    start_date_obj = datetime.strptime(start_date_str, "%m-%d-%Y").date()
    data_sensor = data[data['sensor_id'] == sensor_id]
    data_sensor['date'] = data_sensor['timestamp'].apply(lambda x: x.date())
    if request.args.get('end_date'):
        end_date_str = request.args['end_date']
        end_date_obj = datetime.strptime(end_date_str, "%m-%d-%Y").date()
        data_sensor = data_sensor[data_sensor['date'] >= start_date_obj]
        data_sensor = data_sensor[data_sensor['date'] <= end_date_obj]
    else:
        data_sensor = data_sensor[data_sensor['date'] == start_date_obj]
    grouped = data_sensor.groupby('date').sum()
    output_date = []
    for key, val in grouped['id'].items():
        output_date.append({
            'date': key.strftime('%Y-%m-%d'),
            'total': int(grouped['total'][key])
        })
    res = json.dumps(output_date)
    return res


@app.route("/login", methods=["POST"])
def login():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    user = User.query.filter_by(username=username).one_or_none()
    if not user or not user.check_password(password):
        return jsonify("Wrong username or password"), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)


def prepare_data(df):
    raw = df.to_dict()
    data = []
    for key, val in raw['id'].items():
        data.append({
            'id': val,
            'timestamp': raw['timestamp'][key].strftime('%Y-%m-%d %H:%M'),
            'total': raw['total'][key],
            'sensor_id': raw['sensor_id'][key],
        })
    return data


def read_logs():
    """
    Read all files csv in folder data.
    :return:
    """
    file_ext = ".csv"
    abspath_data = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
    data = []
    for file in os.listdir(abspath_data):
        if not file.endswith(file_ext):
            continue
        with open(os.path.join(abspath_data, file)) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            next(csv_reader)
            for row in csv_reader:
                data.append(row)
        pass
    return data


def main():
    data = read_logs()
    df_data = pd.DataFrame(data, columns=['id', 'timestamp', 'sensor_id', 'total'])
    df_data['total'] = df_data['total'].apply(lambda x: int(x))
    df_data['sensor_id'] = df_data['sensor_id'].apply(lambda x: int(x))
    df_data['id'] = df_data['id'].apply(lambda x: int(x))
    df_data['timestamp'] = df_data['timestamp'].apply(lambda x: datetime.strptime(x, "%m/%d/%Y %H:%M"))
    return df_data


if __name__ == '__main__':
    db.create_all()
    db.session.add(User(full_name="Bruce Wayne", username="batman"))
    db.session.commit()

    app.run()
